#include <jni.h>
#include <dlfcn.h>
#include <android/log.h>
 #include <gtest/gtest.h>

#include "MyCodec.h"

#define LOG_TAG "libmyvideogtest"
#define LOGI(...)  {__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__);}
#define VFILE "/storage/emulated/0/samples/SampleVideo_1080x720_1mb.mp4"


class FixtureBase : public testing::Test{
public:
	FixtureBase(){
		void *loadLib = dlopen("myvideo", RTLD_NOW);
		if (!loadLib){
			LOGI("Could not load myvideo library");
		}
	}
	void SetUp() {
		ObjMC = new MyCodec();
		ObjMC->init(VFILE);
	}
	void TearDown(){
		ObjMC->deinit();
		delete ObjMC;
		ObjMC = 0;
	}

	MyCodec *ObjMC;
};

class MyVideoTestFixture : public FixtureBase
{
public:
};

TEST_F (MyVideoTestFixture, init) {
	int x = 1;
	LOGI("init test ....");
    EXPECT_EQ(true, x == 1);
  }

TEST_F (MyVideoTestFixture, checkDuration) {
	int dur= ObjMC->getDuration();
	LOGI("duration test .... : %d", dur);
    EXPECT_EQ(true, dur > 0);
  }
