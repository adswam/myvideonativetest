
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := MyVideo
LOCAL_MODULE_PATH := $(LOCAL_PATH)/../../MyVideo/libs/armeabi
LOCAL_SRC_FILES := ../$(LOCAL_MODULE_PATH)/libmyvideo.so
include $(PREBUILT_SHARED_LIBRARY)


include $(CLEAR_VARS)

LOCAL_MODULE    := MyVideoNativeTest
LOCAL_SRC_FILES := MyVideoNativeTest.cpp gTestNative.cpp
LOCAL_LDLIBS := -llog -L$(LOCAL_PATH)/../libs/armeabi/ -lmyvideo
LOCAL_STATIC_LIBRARIES := googletest_main
LOCAL_CFLAGS += -DANDROID
LOCAL_CPPFLAGS += -DANDROID

include $(BUILD_SHARED_LIBRARY)

$(call import-module,third_party/googletest)
