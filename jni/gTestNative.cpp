#include <jni.h>
 #include <gtest/gtest.h>
//#include <gmock/gmock.h>

#include <android/log.h>
#define LOG_TAG "libmyvideo"
#define LOGI(...)  {__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__);}

#ifdef __cplusplus
extern "C" {
#endif


JNIEXPORT jint JNICALL Java_com_example_myvideonativetest_MainActivity_runTests
  (JNIEnv *, jclass);

#ifdef __cplusplus
}
#endif

jint JNICALL Java_com_example_myvideonativetest_MainActivity_runTests(JNIEnv* env, jclass clazz)
{
	int argc = 0;
	char* argv[] = {};
	LOGI("natie run gtest .....");
	::testing::GTEST_FLAG(output) = "xml:/sdcard/mytest-report.xml";
	::testing::InitGoogleTest(&argc,(char**)argv);
	return RUN_ALL_TESTS();
}




